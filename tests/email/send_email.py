import unittest
from unittest.mock import patch, MagicMock
from utils import (
    send_email_first_access_password,
    send_email_reset_password,
    send_email
)

class TestEmailFunctions(unittest.TestCase):

    @patch('utils.verify_email_is_valid')
    @patch('utils.send_email')
    def test_send_email_first_access_password(self, mock_send_email, mock_verify_email):
        user_name = "Henrique Torres"
        recipient_email = "henriquetlandin@gmail.com"
        link_to_reset_password_page = "http://example.com/reset"

        send_email_first_access_password(user_name, recipient_email, link_to_reset_password_page)

        mock_verify_email.assert_called_once_with(recipient_email)
        mock_send_email.assert_called_once()

    @patch('utils.verify_email_is_valid')
    @patch('utils.send_email')
    def test_send_email_reset_password(self, mock_send_email, mock_verify_email):
        user_name = "Henrique Torres"
        recipient_email = "henriquetlandin@gmail.com"
        link_to_reset_password_page = "http://example.com/reset"

        send_email_reset_password(user_name, recipient_email, link_to_reset_password_page)

        mock_verify_email.assert_called_once_with(recipient_email)
        mock_send_email.assert_called_once()

    @patch('smtplib.SMTP')
    def test_send_email(self, mock_smtp):

        sender_email = "henriquetlandin@gmail.com"
        sender_password = "senha123"
        recipient_email = "teste@gmail.com"
        title = "Teste"
        text_body = "Corpo do email"

        # Call the function
        send_email(sender_email, sender_password, recipient_email, title, text_body)

        # Assertions
        mock_smtp.return_value.starttls.assert_called_once()
        mock_smtp.return_value.login.assert_called_once_with(sender_email, sender_password)
        mock_smtp.return_value.sendmail.assert_called_once_with(sender_email, [recipient_email], MagicMock().encode())

if __name__ == '__main__':
    unittest.main()

